﻿using System;

namespace Auth.JWT.Core
{
    public interface IApplicationUserManager
    {
        bool ValidateUserLogin(string username, string password);

        ApplicationUser GetUser(Guid userId);

        ApplicationUser GetUser(string username, string password);

        void CreateUser(ApplicationUser user, string password);
    }
}
