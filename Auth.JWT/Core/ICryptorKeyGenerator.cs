﻿namespace Auth.JWT.Core
{
    public interface ICryptorKeyGenerator
    {
        string GenerateKey();


    }
}
