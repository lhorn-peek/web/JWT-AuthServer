﻿using System;
using System.Collections.Generic;

namespace Auth.JWT.Core
{
    public class ApplicationUser
    {

        public Guid UserId { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public List<string> Roles { get; set; }
    }
}
