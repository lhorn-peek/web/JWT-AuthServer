﻿namespace Auth.JWT.Core
{
    public interface IApplicationStore
    {

        ApplicationResource AddApplication(string appName);

        ApplicationResource FindApplication(string appId);

        void EnableApplication(string appId);

        void DisableApplication(string appId);
    }
}
