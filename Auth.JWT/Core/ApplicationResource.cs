﻿namespace Auth.JWT.Core
{
    public class ApplicationResource
    {
        public string ApplicationId { get; set; }

        public string Secret { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

    }
}
