﻿namespace Auth.JWT.Core
{
    public interface IUserRoleService
    {
        bool ValidateClientRoles(ApplicationUser appUser, string clientId);
    }
}
