﻿using System.Collections.Generic;
using Auth.JWT.Core;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Auth.JWT.Extensions.Owin
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        #region init

        private readonly IApplicationStore _applicationStore;
        private readonly IApplicationUserManager _userManager;

        public ApplicationOAuthProvider(IApplicationStore applicationStore, IApplicationUserManager userManager)
        {
            _applicationStore = applicationStore;
            _userManager = userManager;
        }

        #endregion

        private const string ApplicationPropertyKey = "appresource";

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string appId;
            string appSecret;
            string symmetricKeyAsBase64 = string.Empty;

            if (!context.TryGetBasicCredentials(out appId, out appSecret))
            {
                context.TryGetFormCredentials(out appId, out appSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_clientId", "client_Id is not set");
                return Task.FromResult<object>(null);
            }

            var app = _applicationStore.FindApplication(context.ClientId);

            if (app == null)
            {
                context.SetError("invalid_clientId", $"Invalid client_id '{context.ClientId}'");
                return Task.FromResult<object>(null);
            }

            if (!app.IsActive)
            {
                context.SetError("client_restricted", $"restricted access for client_Id '{context.ClientId}'");
                return Task.FromResult<object>(null);
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }


        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var appUser = _userManager.GetUser(context.UserName, context.Password);

            if (appUser == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect");
                return Task.FromResult<object>(null);
            }

            ////Checking if the User is active PA
            //var member = Membership.GetUser(appUser.UserName);
            //if (!member.IsApproved)
            //{
            //    context.SetError("invalid_grant", "This account is not active");
            //    return Task.FromResult<object>(null);
            //}

            //// check to see if allowed into applicataion

            //var userAuthorisedForApp = _roleService.ValidateClientRoles(appUser, context.ClientId);

            //if (!userAuthorisedForApp)
            //{
            //    context.SetError("invalid_grant", "This account it not authorised to access this application.");
            //    return Task.FromResult<object>(null);
            //}

            var identity = new ClaimsIdentity("JWT");

            identity.AddClaim(new Claim(ClaimTypes.Name, appUser.UserName));
            identity.AddClaim(new Claim("sub", appUser.UserName));

            foreach (var role in appUser.Roles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                         ApplicationPropertyKey, context.ClientId ?? string.Empty
                    },
                    {
                        "Email", appUser.Email
                    },
                    {
                        "UserId", appUser.UserId.ToString()
                    }
                });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
    }
}
