﻿using System;
using Auth.JWT.Core;
using Microsoft.Owin.Security;
using Thinktecture.IdentityModel.Tokens;
using System.IdentityModel.Tokens;

namespace Auth.JWT.Extensions.Owin
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        #region init

        private readonly string _authServerUrl;
        private readonly IApplicationStore _applicationStore;

        public CustomJwtFormat(string authServerUrl, IApplicationStore applicationStore)
        {
            _authServerUrl = authServerUrl;
            _applicationStore = applicationStore;
        }

        #endregion

        private const string ApplicationPropertyKey = "appresource";

        public string Protect(AuthenticationTicket data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var appId = data.Properties.Dictionary.ContainsKey(ApplicationPropertyKey) ? data.Properties.Dictionary[ApplicationPropertyKey] : null;

            if (string.IsNullOrWhiteSpace(appId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include spafaxapp");

            var app = _applicationStore.FindApplication(appId);

            var secretKey = app.Secret;

            var keyByteArray = Convert.FromBase64String(secretKey); 

            var signingKey = new HmacSigningCredentials(keyByteArray);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_authServerUrl, appId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new System.NotImplementedException();
        }
    }
}
