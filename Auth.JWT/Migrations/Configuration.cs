using Auth.JWT.Core;
using Auth.JWT.Infrastructure.Sql;

namespace Auth.JWT.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Auth.JWT.Infrastructure.Sql.AuthDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Auth.JWT.Infrastructure.Sql.AuthDbContext context)
        {
            context.Users.AddOrUpdate(
                u => u.UserId,
                new ApplicationUserDbEntity { Email = "user1@email.com", Roles = "Admin,Superuser", UserName = "user1", Password = "password", UserId = Guid.NewGuid() }
            );

            //context.Applications.AddOrUpdate(
            //    a => a.ApplicationId,
            //    new ApplicationResource { ApplicationId = Guid.NewGuid().ToString("N"), IsActive = true, Name = "apiresource", Secret = }
            //);
        }
    }
}
