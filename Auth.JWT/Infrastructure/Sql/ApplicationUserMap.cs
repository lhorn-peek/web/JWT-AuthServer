﻿using System.Data.Entity.ModelConfiguration;

namespace Auth.JWT.Infrastructure.Sql
{
    public class ApplicationUserMap : EntityTypeConfiguration<ApplicationUserDbEntity>
    {
        public ApplicationUserMap()
        {
            ToTable("Users");

            HasKey(a => a.UserId);

            Property(a => a.Email).IsRequired();
            Property(a => a.UserName).IsRequired();
            Property(a => a.Password).IsRequired();

            Property(a => a.Roles).IsOptional();
        }
    }
}
