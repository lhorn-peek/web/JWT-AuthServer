﻿using System.Data.Entity;
using Auth.JWT.Core;

namespace Auth.JWT.Infrastructure.Sql
{
    public class AuthDbContext : DbContext
    {
        public AuthDbContext()
            :base("Name=AuthDbContext")
        {
            Configuration.LazyLoadingEnabled = false;

            Database.SetInitializer(new CreateDatabaseIfNotExists<AuthDbContext>());
        }


        public DbSet<ApplicationResource> Applications { get; set; }

        public DbSet<ApplicationUserDbEntity> Users { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApplicationResourceMap());

            modelBuilder.Configurations.Add(new ApplicationUserMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
