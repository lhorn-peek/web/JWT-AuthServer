﻿using System;
using System.Data.Entity;
using Auth.JWT.Core;

namespace Auth.JWT.Infrastructure.Sql
{
    public class ApplicationStore : IApplicationStore
    {
        #region init

        private readonly AuthDbContext _authDb;
        private readonly ICryptorKeyGenerator _cryptorKeyGenerator;

        public ApplicationStore(AuthDbContext authDb, ICryptorKeyGenerator cryptorKeyGenerator)
        {
            _authDb = authDb;
            _cryptorKeyGenerator = cryptorKeyGenerator;
        }

        #endregion


        public ApplicationResource AddApplication(string appName)
        {
            if (string.IsNullOrEmpty(appName))
                throw new ArgumentNullException(nameof(appName));

            var appId = Guid.NewGuid().ToString("N");

            var appSecret = _cryptorKeyGenerator.GenerateKey();

            var newApp = new ApplicationResource { ApplicationId = appId, Name = appName, Secret = appSecret, IsActive = false };

            _authDb.Applications.Add(newApp);
            _authDb.SaveChanges();

            return newApp;
        }

        public ApplicationResource FindApplication(string appId)
        {
            var app = _authDb.Applications.Find(appId);

            return app;
        }

        public void EnableApplication(string appId)
        {
            SetIsActiveState(appId, true);
        }

        public void DisableApplication(string appId)
        {
            SetIsActiveState(appId, false);
        }


        private void SetIsActiveState(string appId, bool isActive)
        {
            var app = _authDb.Applications.Find(appId);

            if (app == null) throw new ArgumentException("app doesnt exist.");

            app.IsActive = isActive;

            _authDb.Entry(app).State = EntityState.Modified;
            _authDb.Applications.Attach(app);

            _authDb.SaveChanges();
        }
    }
}
