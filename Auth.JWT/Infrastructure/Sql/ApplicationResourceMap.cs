﻿using System.Data.Entity.ModelConfiguration;
using Auth.JWT.Core;

namespace Auth.JWT.Infrastructure.Sql
{
    public class ApplicationResourceMap : EntityTypeConfiguration<ApplicationResource>
    {
        public ApplicationResourceMap()
        {
            ToTable("Applications");

            HasKey(a => a.ApplicationId);

            Property(a => a.IsActive).IsRequired();
            Property(a => a.Name).IsRequired();
            Property(a => a.Secret).IsRequired();
        }
    }
}
