﻿using System;
using System.Linq;
using Auth.JWT.Core;

namespace Auth.JWT.Infrastructure.Sql
{
    /* This is an example and should NOT be used for production. */

    public class AppUserManagerSimple : IApplicationUserManager
    {
        #region init

        private readonly AuthDbContext _authDb;

        public AppUserManagerSimple(AuthDbContext authDb)
        {
            _authDb = authDb;
        }

        #endregion

        public bool ValidateUserLogin(string username, string password)
        {
            var user = GetUser(username, password);

            return user != null;
        }

        public ApplicationUser GetUser(Guid userId)
        {
            var dbUser = _authDb.Users.Find(userId);

            return MapFrom(dbUser);
        }

        public ApplicationUser GetUser(string username, string password)
        {
            var dbUser = _authDb.Users.AsNoTracking().FirstOrDefault(u => u.UserName == username & u.Password == password);

            return MapFrom(dbUser);
        }

        public void CreateUser(ApplicationUser user, string password)
        {
            var dbUser = NewEntity(user, password);

            _authDb.Users.Add(dbUser);
            _authDb.SaveChanges();
        }


        #region private

        private static ApplicationUser MapFrom(ApplicationUserDbEntity userEntity)
        {
            if (userEntity == null) return null;

            return new ApplicationUser
            {
                UserName = userEntity.UserName,
                Email = userEntity.Email,
                UserId = userEntity.UserId,
                Roles = userEntity.Roles?.Split(',').ToList()
            };
        }

        private static ApplicationUserDbEntity NewEntity(ApplicationUser user, string password)
        {
            return new ApplicationUserDbEntity
            {
                UserName = user.UserName,
                Password = password,
                Email = user.Email,
                UserId = user.UserId != Guid.Empty ? user.UserId : Guid.NewGuid(),
                Roles = string.Join(",", user.Roles)
            };
        }

        #endregion
    }
}
