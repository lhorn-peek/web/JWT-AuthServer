﻿using System;
using System.Security.Cryptography;
using Auth.JWT.Core;

namespace Auth.JWT.Infrastructure
{
    public class SimpleKeyGenerator : ICryptorKeyGenerator
    {
        public string GenerateKey()
        {
            var key = new byte[32];

            RandomNumberGenerator.Create().GetBytes(key);

            var encKey = Convert.ToBase64String(key);  //TextEncodings.Base64Url.Encode(key);

            return encKey;
        }
    }
}
