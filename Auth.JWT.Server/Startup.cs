﻿using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Auth.JWT.Core;
using Auth.JWT.Extensions.Owin;
using Auth.JWT.Infrastructure;
using Auth.JWT.Infrastructure.Sql;

namespace Auth.JWT.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes();

            ConfigureOAuth(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);

        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            ICryptorKeyGenerator keyGenerator = new SimpleKeyGenerator();
            var authDb = new AuthDbContext();

            IApplicationStore applicationStore = new ApplicationStore(authDb, keyGenerator);
            IApplicationUserManager userManager = new AppUserManagerSimple(authDb);

            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(210),
                Provider = new ApplicationOAuthProvider(applicationStore, userManager),
                AccessTokenFormat = new CustomJwtFormat("app.auth.oauth", applicationStore)
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);

        }
    }
}