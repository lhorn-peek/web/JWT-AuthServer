﻿using System.Web.Http;
using Auth.JWT.Core;
using Auth.JWT.Infrastructure;
using Auth.JWT.Infrastructure.Sql;
using Auth.JWT.Server.Dtos;

namespace Auth.JWT.Server.Controllers
{
    [RoutePrefix("api/applications")]
    public class ApplicationsController : ApiController
    {
        #region init

        private readonly IApplicationStore _applicationStore;

        public ApplicationsController()
        {
            ICryptorKeyGenerator keyGenerator = new SimpleKeyGenerator();

            var authDb = new AuthDbContext();

            _applicationStore = new ApplicationStore(authDb, keyGenerator);
        }

        #endregion


        [Route("new")]
        public IHttpActionResult Post(NewAppDto app)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var newApp = _applicationStore.AddApplication(app.Name);

            return Ok($"New app registered and is pending approval. Your Application Id is: '{newApp.ApplicationId}'");

        }
    }
}
