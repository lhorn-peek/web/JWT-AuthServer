﻿using System.ComponentModel.DataAnnotations;

namespace Auth.JWT.Server.Dtos
{
    public class NewAppDto
    {
        [Required]
        public string Name { get; set; }
    }
}